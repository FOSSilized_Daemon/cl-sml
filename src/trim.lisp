(in-package :sml)

;;;; The check.lisp file implements variables, functions, and other
;;;; functionality for performing trimming operations in ANSI
;;;; compliant Common Lisp. The entirety of the below codebase
;;;; is written to be easily understood and reprogrammable
;;;; from any ANSI compliant Common Lisp implementation.

(defvar *whitespace* '(#\Space    #\Newline #\Backspace #\Tab
                       #\Linefeed #\Page    #\Return    #\Rubout)
  "Whitespace designators.")

(defun trim-left (target-string &optional (target-item *whitespace*))
  "Removes TARGET-ITEM at the beginning of TARGET-STRING (whitespace by default)."
    (string-left-trim target-item target-string))

(defun trim-right (target-string &optional (target-item *whitespace*))
  "Removes TARGET-ITEM at the end of TARGET-STRING (whitespace by default)."
    (string-right-trim target-item target-string))

(defun trim (target-string &optional (target-item *whitespace*))
  "Removes TARGET-ITEM at the beginning and end of TARGET-STRING (whitespace by default)."
    (trim-left (trim-right target-string target-item) target-item))

(defun collapse-whitespace (target-string)
  "Ensures there is only one space between sub-strings in TARGET-STRING."
  (ppcre:regex-replace-all "\\s+" target-string " "))

(defun strip-first-char (target-string)
  "Removes the first character from TARGET-STRING."
  (substring target-string :index-range (list '1 (1- (length target-string)))))

(defun strip-last-char (target-string)
  "Removes the last character from TARGET-STRING."
  (substring target-string :index-range (list '0 (- (length target-string) 1))))

(defun strip-first-char-p (target-string target-character &key (ignore-case t))
  "Removes TARGET-CHARACTER from TARGET-STRING if it is the first character with case-senitivity (disabled by
  default)."
  (when (starting-char-p target-string target-character :ignore-case ignore-case)
    (substring target-string :index-range (list '1 (1- (length target-string))))))

(defun strip-last-char-p (target-string target-character &key (ignore-case t))
  "Removes TARGET-CHARACTER from TARGET-STRING if it is the last character with case-senitivity (disabled by
  default)."
  (when (ending-char-p target-string target-character :ignore-case ignore-case)
    (substring target-string :index-range (list '0 (- (length target-string) 1)))))

(defun shorten (target-string target-length &key (ellipsis "..."))
  "Shortens TARGET-STRING below TARGET-LENGTH and appends ELLIPSIS ('...' by default)."
  (if (< target-length (length target-string))
    (let ((new-string (concatenate 'string (subseq target-string 0 (max (- target-length (length ellipsis)) 0))
                                   ellipsis)))
      (format nil "~A" new-string))))

(defun fit-string (target-string target-length &key (target-character #\Space) (ellipsis "...") (target-side :right))
  "Fits TARGET-STRING to TARGET-LENGTH by either collpasing it with ELLIPSIS, if TARGET-SIDE is :RIGHT, default, or
   by adding padding using TARGET-CHARACTER, space by default, is TARGET-SIDE is :LEFT."
   (let ((target-string-length (length target-string))
         (new-target-character (coerce target-character 'character)))
     (cond
       ((= target-string-length target-length)
        target-string)
       ((> target-string-length target-length)
        (shorten target-string target-length :ellipsis ellipsis))
       ((< target-string-length target-length)
        (pad target-string target-length :target-character new-target-character :target-side target-side)))))

(defun strip-punctuation (target-string &key (replacement-character ""))
  "Removes puncutation marks from TARGET-STRING and replaces them with REPLACEMENT-CHARACTER (disabled by default)."
  (ppcre:regex-replace-all "[!\"#\＄%&\'\(\)\*\+,-\./:;<=>\?@\[\\\]\^_`{\|}~]" target-string replacement-character
                           :preserve-case t))
