(in-package :sml)

;;;; The concatenation.lisp file implements variables, functions,
;;;; and other functionality for performing concatenation operations
;;;; on strings in ANSI compliant Common Lisp. The entirety of the
;;;; below codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp implementation.

(defun concatenate-string (&rest string-list)
  "Concatenates all of the strings in STRING-LIST into one
   string."
   (let ((new-string nil))
     (loop for current-string in string-list
        do (setf new-string (concatenate 'string (when (not (string-equal new-string nil))
                                                  new-string)
                                         current-string)))
     new-string))

(defun join (string-list delimiter)
  "Concatenates all of the strings in STRING-LIST into one
   string with DELIMITER between them."
   (let ((new-string nil))
     (loop for current-string in string-list
        do (setf new-string (concatenate-string (when (not (string-equal new-string nil))
                                                  new-string)
                                                  current-string delimiter)))
     new-string))

(defun join-lines (string-list)
  "Join STRING-LIST with a newline character."
  (join string-list (make-string 1 :initial-element #\Newline)))

(defun append-string (target-string sub-string)
  "Appends SUB-STRING to TARGET-STRING."
  (concatenate-string target-string sub-string))

(defun prepend-string (target-string sub-string)
  "Prepends SUB-STRING to TARGET-STRING."
  (concatenate-string sub-string target-string))

(defun ensure-first-char (target-string target-character)
  "Prepends TARGET-CHARACTER to TARGET-STRING if it is not
   already there."
  (when (not (starting-char-p target-string target-character))
    (prepend-string target-string target-character)))

(defun ensure-last-char (target-string target-character)
  "Appends TARGET-CHARACTER to TARGET-STRING if it is not
   already there."
  (when (not (ending-char-p target-string target-character))
    (append-string target-string target-character)))

(defun repeated-concatenation (target-string &key (repeat-times 2))
  "Repeatedly concatenate TARGET-STRING REPEAT-TIMES
   (2 by default)."
  (let ((new-string nil))
    (dotimes (iterator repeat-times)
      (setf new-string (cons target-string new-string)))
    (apply #'concatenate-string new-string)))

(defun add-prefix (string-list target-prefix)
  "Prepends TARGET-PREFIX to each string in STRING-LIST."
  (mapcar #'(lambda (target-string)
              (prepend-string target-string target-prefix)) string-list))

(defun add-suffix (string-list target-suffix)
  "Appends TARGET-SUFFIX to each string in STRING-LIST."
  (mapcar #'(lambda (target-string)
              (append-string target-string target-suffix)) string-list))

(defun pad (target-string target-length &key (target-character " ")
                                             (target-side :right))
  "Pads TARGET-STRING with TARGET-CHARACTER, space by
   default, on TARGET-SIDE, right by default, until
   it is TARGET-LENGTH."
  (let ((target-string-length (length target-string))
        (target-character-string (string target-character)))
    (when (> target-length target-string-length)
      target-string)

    (case target-side
      (:right
        (let ((new-string target-string))
          (loop while (< (length new-string)  target-length)
                do (setf new-string (concatenate 'string new-string target-character-string)))
          new-string))
      (:left
        (let ((new-string target-string))
          (loop while (< (length new-string)  target-length)
                do (setf new-string (concatenate 'string target-character-string new-string)))
          new-string))
      (:center
        (multiple-value-bind (first-half second-half)
          (floor (- target-length target-string-length) 2)
          (concatenate 'string
                       (make-string first-half :initial-element (coerce target-character-string 'character))
                       target-string
                       (make-string (+ first-half second-half) :initial-element (coerce target-character-string 'character))))))))

(defun pad-right (target-string target-length &key (target-character #\Space))
  "Prepends TARGET-CHARACTER, space by default,
   to TARGET-STRING until it is TARGET-LENGTH."
  (pad target-string target-length :target-character target-character :target-side :right))

(defun pad-left (target-string target-length &key (target-character #\Space))
  "Appends TARGET-CHARACTER, space by default,
   to TARGET-STRING until it is TARGET-LENGTH."
  (pad target-string target-length :target-character target-character :target-side :left))

(defun pad-center (target-string target-length &key (target-character #\Space))
  "Adds TARGET-CHARACTER, space by default,
   to TARGET-STRING's center until it is TARGET-LENGTH."
  (pad target-string target-length :target-character target-character :target-side :center))
