(in-package :sml)

;;;; The substring.lisp file implements variables, functions,
;;;; and other functionality for performing substring operations
;;;; on strings in ANSI compliant Common Lisp. The entirety of the
;;;; below codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp implementation.

(defun substring (target-string &key (index-range nil))
  "Returns SUB-STRING within INDEX-RANGE in TARGET-STRING."
  (when index-range
    (subseq target-string (car index-range)
                          (cadr index-range))))

(defun first-substring (target-string)
  "Returns the first substring of TARGET-STRING."
  (substring target-string :index-range '(0 1)))

(defun last-substring (target-string)
  "Returns the last substring of TARGET-STRING."
  (substring target-string :index-range (list (1- (length target-string))
                                              (length target-string))))

(defun rest-of-substring (target-string)
  "Returns the rest substring of TARGET-STRING."
  (subseq target-string 1))

(defun nth-of-substring (target-string target-occurence)
  "Returns the TARGET-OCCURENCE substring of TARGET-STRING."
  (if (= target-occurence 0)
    (first-substring target-string)
    (nth-of-substring (rest-of-substring target-string)
                      (1- target-occurence))))

(defun insert-substring (target-string sub-string index)
  "Inserts SUB-STRING into TARGET-STRING at INDEX.
   Unless INDEX is empty, in which case it returns TARGET-STRING."
  (if (or (not index)
          (> index (length target-string)))
    sub-string

    (progn
      (if (characterp sub-string)
        (setf sub-string (string sub-string)))))

  (concatenate 'string (subseq target-string 0 index)
               sub-string
               (subseq target-string index)))

(defun replace-substring-with-string (target-string old-string new-string &key (first-occurence nil))
  "Replaces either all occurences of OLD-STRING with
   NEW-STRING in TARGET-STRING or only the FIRST-OCCURENCE."
  (let* ((ppcre:*allow-quoting* t)
         (old-string (concatenate-string "\\Q" old-string)))
    (if (not first-occurence)
      (ppcre:regex-replace-all old-string target-string (list new-string))
      (ppcre:regex-replace old-string target-string (list new-string)))))

(defun replace-first-substring-with-string (target-string old-string new-string)
  "Replaces the first occurence of OLD-STRING with
   NEW-STRING in TARGET-STRING."
  (replace-substring-with-string target-string old-string new-string :first-occurence t))

(defun find-prefix (first-string second-string)
  "Finds the common prefix between the FIRST-STRING
   and the SECOND-STRING."
  (subseq first-string 0 (or (mismatch first-string second-string) (length first-string))))

(defun find-suffix (first-string second-string)
  "Finds the common suffix between FIRST-STRING
   and SECOND-STRING."
  (subseq first-string (or (mismatch first-string second-string :from-end t) 0)))

(defun count-substring (target-string substring &key (index-range '(0 nil)))
  "Returns the non-overlapping occurences of SUBSTRING
   in TARGET-STRING within INDEX-RANGE (0 to NIL by default)."
  (let ((index-start (car index-range))
        (index-end   (cadr index-range))
        (sub-string-length (length substring)))
    (loop :for current-position := (search substring target-string :start2 index-start :end2 index-end)
          :then (search substring target-string :start2 (+ current-position sub-string-length) :end2 index-end)
          :while (not (null current-position))
          :summing 1)))
