(in-package :sml)

;;;; The metrics.lisp file implements variables, functions, and other
;;;; functionality for performing string calculation operations in ANSI
;;;; compliant Common Lisp. The entirety of the below codebase
;;;; is written to be easily understood and reprogrammable from
;;;; any ANSI compliant Common Lisp implementation.
