(defsystem "sml"
  :name "sml"
  :version "0.1.0"
  :maintainer "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :author "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :license "AGPL (see LISENCE for details)."
  :description "A common-lisp library for manipulating strings."
  :long-description "A common-lisp library for manipulating strings."
  :depends-on ("sll" "cl-ppcre" "cl-ppcre-unicode")
  :components ((:file "package")
               (:file "case")
               (:file "check")
               (:file "concatenation")
               (:file "metrics")
               (:file "split")
               (:file "substring")
               (:file "trim"))
  :in-order-to ((test-op
                  (test-op :sml/tests))))

(defsystem "sml/tests"
  :name "sml/tests"
  :version "0.1.0"
  :maintainer "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :author "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :license "AGPL (see LISENCE for details)."
  :description "cl-sml test suite."
  :long-description "cl-sml test suite."
  :depends-on ("sml")
  :components ((:file "tests/package")
               (:file "tests/check-test")
               (:file "tests/concatenation-test")
               (:file "tests/split-test")
               (:file "tests/substring-test")
               (:file "tests/trim-test")
               (:file "tests/sml-test"))
  :perform (test-op (op c)
                    (symbol-call '#:sml/tests :test/sml)))
