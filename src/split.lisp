(in-package :sml)

;;;; The split.lisp file implements variables, functions,
;;;; and other functionality for performing split operations
;;;; on strings in ANSI compliant Common Lisp. The entirety of the
;;;; below codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp implementation.

(defun split-string (target-string delimiter &key (omit-nulls nil)
                                                  target-limit
                                                  (index-range '(0 nil)))
  "Splits TARGET-STRING into multiple strings by DELIMITER
   at INDEX-RANGE."
  (let* ((limit (or target-limit
                    (1+ (length target-string))))
         (result (ppcre:split (string delimiter) target-string :limit limit
                                                               :start (car index-range)
                                                               :end   (cadr index-range))))

    (if omit-nulls
      (remove-if (lambda (target)
                   (empty-p target)) result)
      result)))

(defun reverse-split-string (target-string delimiter &key (omit-nulls nil)
                                                          target-limit)
  "Splits TARGET-STRING into multiple strings by DELIMITER
   in reverse."
  (reverse (split-string (string target-string) delimiter
                        :omit-nulls omit-nulls
                        :target-limit target-limit)))

(defun split-with-omit-nulls (target-string delimiter)
  "Splits TARGET-STRING into multiple strings by DELIMITER
   with :OMIT-NULLS."
  (split-string target-string delimiter :omit-nulls t))

(defun string-to-words (target-string &key (target-limit 0))
  "Splits TARGET-STRING into a list of words using spaces
   as the delimiter."
  (ppcre:split "\\s+" (trim-left target-string) :limit target-limit))

(defun words-to-string (string-list)
  "Joins all words in STRING-LIST into one string using
   spaces as the delimiter."
  (join string-list " "))

(defun split-by-lines (target-string &key (omit-nulls nil))
  "Split TARGET-STRING by the newline character and return
   a list of lines."
  (let ((end (if (eql #\Newline (elt target-string (1- (length target-string))))
               (1- (length target-string))
               nil)))
    (split-string target-string #\Newline :omit-nulls omit-nulls
                                          :index-range (list 0 end))))
