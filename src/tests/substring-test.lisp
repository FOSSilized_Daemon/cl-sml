(in-package :sml/tests)

;;;; The substring-test.lisp file implements rigorous checks
;;;; for all variables, functions, etc. within substring.lisp
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun test/substring ()
  "Runs all possible tests for (SUBSTRING)."
  (when (not (sml:substring "Hello world" :index-range '(1 6)))
    (error "(SUBSTRING) failed tests.")))

(defun test/first-substring ()
  "Runs all possible tests for (FIRST-SUBSTRING)."
  (when (not (sml:first-substring "Hello"))
    (error "(FIRST-SUBSTRING) failed tests.")))

(defun test/last-substring ()
  "Runs all possible tests for (LAST-SUBSTRING)."
  (when (not (sml:first-substring "Hello"))
    (error "(LAST-SUBSTRING) failed tests.")))

(defun test/rest-of-substring ()
  "Runs all possible tests for (REST-OF-SUBSTRING)."
  (when (not (sml:rest-of-substring "Hello"))
    (error "(REST-OF-SUBSTRING) failed tests.")))

(defun test/nth-of-substring ()
  "Runs all possible tests for (NTH-OF-SUBSTRING)."
  (when (not (sml:nth-of-substring "Hello, my friend, hello" 2))
    (error "(NTH-OF-SUBSTRING) failed tests.")))

(defun test/insert-substring ()
  "Runs all possible tests for (INSERT-SUBSTRING)."
  (when (not (sml:insert-substring "Hello" "world" 5))
    (error "(INSERT-SUBSTRING) failed tests.")))

(defun test/replace-substring-with-string ()
  "Runs all possible tests for (REPLACE-SUBSTRING-WITH-STRING)."
  (cond
    ((not (sml:replace-substring-with-string "Hello friend" "friend" "world"))
      (error "(REPLACE-SUBSTRING-WITH-STRING) failed test for generic call."))
    ((not (sml:replace-substring-with-string "Hello friend. How are you friend?" "friend" "world" :first-occurence t))
      (error "(REPLACE-SUBSTRING-WITH-STRING) failed test for non-generic call."))))

(defun test/replace-first-substring-with-string ()
  "Runs all possible tests for (REPLACE-FIRST-SUBSTRING-WITH-STRING)."
  (when (not (sml:replace-first-substring-with-string "Hello friend" "friend" "world"))
    (error "(REPLACE-FIRST-SUBSTRING-WITH-STRING) failed tests.")))

(defun test/find-prefix ()
  "Runs all possible tests for (FIND-PREFIX)."
  (when (not (sml:find-prefix "Hello" "Helium"))
    (error "(FIND-PREFIX) failed tests.")))

(defun test/find-suffix ()
  "Runs all possible tests for (FIND-SUFFIX)."
  (when (not (sml:find-suffix "Hello" "Heliumo"))
    (error "(FIND-SUFFIX) failed tests.")))

(defun test/count-substring ()
  "Runs all possible tests for (COUNT-SUBSTRING)."
  (cond
    ((not (sml:count-substring "Hello world" "world"))
      (error "(COUNT-SUBSTRING) failed test for generic call."))
    ((not (sml:count-substring "Hello world hello world" "world" :index-range '(0 10)))
      (error "(COUNT-SUBSTRING) failed test for non-generic call."))))
