(in-package :sml/tests)

;;;; The check-test.lisp file implements rigorous checks
;;;; for all variables, functions, etc. within check.lisp
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun test/empty-p ()
  "Runs all possible tests for (EMPTY-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "      My other string.     "))
    (cond
      ((not (sml:empty-p string1))
        (error "(EMPTY-P) failed test on STRING1."))
      ((not (sml:empty-p string2))
        (error "(EMPTY-P) failed test on STRING2."))
      ((sml:empty-p string3)
        (error "(EMPTY-P) failed test on STRING3."))
      ((sml:empty-p string4)
        (error "(EMPTY-P) failed test on STRING4.")))))

(defun test/exists-p ()
  "Runs all possible tests for (EXISTS-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "      My other string.     "))
    (cond
      ((sml:exists-p string1)
        (error "(EXISTS-P) failed test on STRING1."))
      ((sml:exists-p string2)
        (error "(EXISTS-P) failed test on STRING2."))
      ((not (sml:exists-p string3))
        (error "(EXISTS-P) failed test on STRING3."))
      ((not (sml:exists-p string4))
        (error "(EXISTS-P) failed test on STRING4.")))))

(defun test/digit-p ()
  "Runs all possible tests for (DIGIT-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "1My other string.")
        (string5 "My fun string.2"))
    (cond
      ((sml:digit-p string1)
        (error "(DIGIT-P) failed test on STRING1."))
      ((sml:digit-p string2)
        (error "(DIGIT-P) failed test on STRING2."))
      ((sml:digit-p string3)
        (error "(DIGIT-P) failed test on STRING3."))
      ((not (sml:digit-p string4))
        (error "(DIGIT-P) failed test on STRING4."))
      ((not (sml:digit-p string5))
        (error "(DIGIT-P) failed test on STRING5.")))))

(defun test/alphanumeric-p () ; TODO: :letters-digits t has not been tested due to issues with {L} in ppcre.
  "Runs all possible tests for (ALPHANUMERIC-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "1My other string.")
        (string5 "My other string2")
        (string6 "1My fun string"))
    (cond
      ((sml:alphanumeric-p string1)
        (error "(ALPHANUMERIC-P) failed test on STRING1."))
      ((sml:alphanumeric-p string2)
        (error "(ALPHANUMERIC-P) failed test on STRING2."))
      ((sml:alphanumeric-p string3)
       (error "(ALPHANUMERIC-P) failed test on STRING3."))
      ((sml:alphanumeric-p string4)
        (error "(ALPHANUMERIC-P) failed test on STRING4."))
      ((not (sml:alphanumeric-p string5))
        (error "(ALPHANUMERIC-P) failed test on STRING5."))
      ((not (sml:alphanumeric-p string6))
        (error "(ALPHANUMERIC-P) failed test on STRING6.")))))

(defun test/has-alphanumeric-p ()
  "Runs all possible tests for (HAS-ALPHANUMERIC-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "1My other string.")
        (string5 "My other string2")
        (string6 "1My fun string"))
    (cond
      ((sml:has-alphanumeric-p string1)
        (error "(HAS-ALPHANUMERIC-P) failed test on STRING1."))
      ((sml:has-alphanumeric-p string2)
        (error "(HAS-ALPHANUMERIC-P) failed test on STRING2."))
      ((not (sml:has-alphanumeric-p string3))
       (error "(HAS-ALPHANUMERIC-P) failed test on STRING3."))
      ((not (sml:has-alphanumeric-p string4))
        (error "(HAS-ALPHANUMERIC-P) failed test on STRING4."))
      ((not (sml:has-alphanumeric-p string5))
        (error "(HAS-ALPHANUMERIC-P) failed test on STRING5."))
      ((not (sml:has-alphanumeric-p string6))
        (error "(HAS-ALPHANUMERIC-P) failed test on STRING6.")))))

(defun test/alpha-p ()
  "Runs all possible tests for (ALPHA-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "1My other string")
        (string5 "1My other string.")
        (string6 "My fun string"))
    (cond
      ((sml:alpha-p string1)
        (error "(ALPHA-P) failed test on STRING1."))
      ((sml:alpha-p string2)
        (error "(ALPHA-P) failed test on STRING2."))
      ((sml:alpha-p string3)
       (error "(ALPHA-P) failed test on STRING3."))
      ((sml:alpha-p string4)
        (error "(ALPHA-P) failed test on STRING4."))
      ((sml:alpha-p string5)
        (error "(ALPHA-P) failed test on STRING5."))
      ((not (sml:alpha-p string6))
        (error "(ALPHA-P) failed test on STRING6.")))))

(defun test/has-alpha-p ()
  "Runs all possible tests for (HAS-ALPHA-P)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "1My other string")
        (string5 "1My other string.")
        (string6 "My fun string"))
    (cond
      ((sml:has-alpha-p string1)
        (error "(HAS-ALPHA-P) failed test on STRING1."))
      ((sml:has-alpha-p string2)
        (error "(HAS-ALPHA-P) failed test on STRING2."))
      ((not (sml:has-alpha-p string3))
       (error "(HAS-ALPHA-P) failed test on STRING3."))
      ((not (sml:has-alpha-p string4))
        (error "(HAS-ALPHA-P) failed test on STRING4."))
      ((not (sml:has-alpha-p string5))
        (error "(HAS-ALPHA-P) failed test on STRING5."))
      ((not (sml:has-alpha-p string6))
        (error "(HAS-ALPHA-P) failed test on STRING6.")))))

(defun test/letter-p ()
  "Runs all possible tests for (LETTER-P)."
  (let ((string1 "1234567890")
        (string2 "123abc")
        (string3 "abc")
        (string4 "abcß"))
    (cond
      ((sml:letter-p string1)
        (error "(LETTER-P) failed test on STRING1."))
      ((sml:letter-p string2)
        (error "(LETTER-P) failed test on STRING2."))
      ((not (sml:letter-p string3))
        (error "(LETTER-P) failed test on STRING3."))
      ((not (sml:letter-p string4))
        (error "(LETTER-P) failed test on STRING4.")))))

(defun test/has-letter-p ()
  "Runs all possible tests for (HAS-LETTER-P)."
  (let ((string1 "1234567890")
        (string2 "123abc")
        (string3 "abc")
        (string4 "abcß"))
    (cond
      ((sml:has-letter-p string1)
        (error "(HAS-LETTER-P) failed test on STRING1."))
      ((not (sml:has-letter-p string2))
        (error "(HAS-LETTER-P) failed test on STRING2."))
      ((not (sml:has-letter-p string3))
        (error "(HAS-LETTER-P) failed test on STRING3."))
      ((not (sml:has-letter-p string4))
        (error "(HAS-LETTER-P) failed test on STRING4.")))))

(defun test/ascii-char-p ()
  "Runs all possible tests for (ASCII-CHAR-P)."
  (let ((character1 #\ß)
        (character2 #\A)
        (character3 #\Z))
    (cond
      ((sml:ascii-char-p character1)
        (error "(ASCII-CHAR-P) failed test on CHARACTER1."))
      ((not (sml:ascii-char-p character2))
        (error "(ASCII-CHAR-P) failed test on CHARACTER2."))
      ((not (sml:ascii-char-p character3))
        (error "(ASCII-CHAR-P) failed test on CHARACTER3.")))))

(defun test/ascii-p ()
  "Runs all possible tests for (ASCII-P)."
  (let ((character1 #\ß)
        (character2 #\A)
        (character3 #\Z)
        (string1 "My string")
        (string2 "My ßring"))
    (cond
      ((sml:ascii-p character1)
        (error "(ASCII-P) failed test on CHARACTER1."))
      ((not (sml:ascii-p character2))
        (error "(ASCII-P) failed test on CHARACTER2."))
      ((not (sml:ascii-p character3))
        (error "(ASCII-P) failed test on CHARACTER3."))
      ((not (sml:ascii-p string1))
        (error "(ASCII-P) failed test on STRING1."))
      ((sml:ascii-p string2)
        (error "(ASCII-P) failed test on STRING2.")))))

(defun test/uppercase-p ()
  "Runs all possible tests for (UPPERCASE-P)."
  (let ((string1 "hello world")
        (string2 "HeLlO wOrLd")
        (string3 "HELLO WORLD"))
    (cond
      ((sml:uppercase-p string1)
        (error "(UPPERCASE-P) failed test on STRING1."))
      ((sml:uppercase-p string2)
        (error "(UPPERCASE-P) failed test on STRING2."))
      ((not (sml:uppercase-p string3))
        (error "(UPPERCASE-P) failed test on STRING3.")))))

(defun test/lowercase-p ()
  "Runs all possible tests for (LOWERCASE-P)."
  (let ((string1 "HELLO WORLD")
        (string2 "HeLlO wOrLd")
        (string3 "hello world"))
    (cond
      ((sml:lowercase-p string1)
        (error "(LOWERCASE-P) failed test on STRING1."))
      ((sml:lowercase-p string2)
        (error "(LOWERCASE-P) failed test on STRING2."))
      ((not (sml:lowercase-p string3))
        (error "(LOWERCASE-P) failed test on STRING3.")))))

(defun test/sub-string-p ()
  "Runs all possible tests for (SUB-STRING-P)."
  (let ((string1    "Hello world")
        (substring1 "test")
        (string2    "Hello real world")
        (substring2 "real")
        (string3    "HELLO WoRlD")
        (substring3 "WoRlD"))
    (cond
      ((sml:sub-string-p string1 substring1)
        (error "(SUB-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((not (sml:sub-string-p string2 substring2))
        (error "(SUB-STRING-P) failed test on STRING2 with SUBSTRING2."))
      ((not (sml:sub-string-p string3 substring3 :ignore-case nil))
        (error "(SUB-STRING-P) failed test on STRING3 with SUBSTRING3.")))))

(defun test/sub-string-index-p ()
  "Runs all possible tests for (SUB-STRING-INDEX-P)."
  (let ((string1    "Hello world")
        (substring1 "test")
        (string2    "Hello real world")
        (substring2 "real")
        (string3    "HELLO WoRlD")
        (substring3 "WoRlD"))
    (cond
      ((sml:sub-string-index-p string1 substring1)
        (error "(SUB-STRING-INDEX-P) failed test on STRING1 with SUBSTRING1."))
      ((not (sml:sub-string-index-p string2 substring2 :index-range '(6 10)))
        (error "(SUB-STRING-INDEX-P) failed test on STRING2 with SUBSTRING2."))
      ((not (sml:sub-string-index-p string3 substring3 :index-range '(6 11) :ignore-case nil))
        (error "(SUB-STRING-INDEX-P) failed test on STRING3 with SUBSTRING3.")))))

(defun test/scan-string-p ()
  "Runs all possible tests for (SCAN-STRING-P)."
  (let ((string1    "Hello")
        (substring1 "world")
        (string2    "Time")
        (substring2 "im"))
    (cond
      ((sml:scan-string-p string1 substring1)
        (error "(SCAN-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((sml:scan-string-p string1 substring1 :index-range t)
        (error "(SCAN-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((not (sml:scan-string-p string2 substring2))
        (error "(SCAN-STRING-P) failed test on STRING2 with SUBSTRING2."))
      ((not (sml:scan-string-p string2 substring2 :index-range t))
        (error "(SCAN-STRING-P) failed test on STRING2 with SUBSTRING2.")))))

(defun test/starting-char-p ()
  "Runs all possible tests for (STARTING-CHAR-P)."
  (let ((string1    "Hello")
        (character1 "e")
        (string2    "World")
        (character2 "W"))
    (cond
      ((sml:starting-char-p string1 character1)
        (error "(STARTING-CHAR-P) failed test on STRING1 with CHARACTER1."))
      ((sml:starting-char-p string1 character1 :ignore-case nil)
        (error "(STARTING-CHAR-P) failed test on STRING1 with CHARACTER1."))
      ((not (sml:starting-char-p string2 character2))
        (error "(STARTING-CHAR-P) failed test on STRING2 with CHARACTER2."))
      ((not (sml:starting-char-p string2 character2 :ignore-case nil))
        (error "(STARTING-CHAR-P) failed test on STRING2 with CHARACTER2.")))))

(defun test/ending-char-p ()
  "Runs all possible tests for (ENDING-CHAR-P)."
  (let ((string1    "Hello")
        (character1 "l")
        (string2    "World")
        (character2 "d"))
    (cond
      ((sml:ending-char-p string1 character1)
        (error "(ENDING-CHAR-P) failed test on STRING1 with CHARACTER1."))
      ((sml:ending-char-p string1 character1 :ignore-case nil)
        (error "(ENDING-CHAR-P) failed test on STRING1 with CHARACTER1."))
      ((not (sml:ending-char-p string2 character2))
        (error "(ENDING-CHAR-P) failed test on STRING2 with CHARACTER2."))
      ((not (sml:ending-char-p string2 character2 :ignore-case nil))
        (error "(ENDING-CHAR-P) failed test on STRING2 with CHARACTER2.")))))

(defun test/starting-sub-string-p ()
  "Runs all possible tests for (STARTING-SUB-STRING-P)."
  (let ((string1    "Hello")
        (substring1 "el")
        (string2    "World")
        (substring2 "Wo"))
    (cond
      ((sml:starting-sub-string-p string1 substring1)
        (error "(STARTING-SUB-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((sml:starting-sub-string-p string1 substring1 :ignore-case nil)
        (error "(STARTING-SUB-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((not (sml:starting-sub-string-p string2 substring2))
        (error "(STARTING-SUB-STRING-P) failed test on STRING2 with SUBSTRING2."))
      ((not (sml:starting-sub-string-p string2 substring2 :ignore-case nil))
        (error "(STARTING-SUB-STRING-P) failed test on STRING2 with SUBSTRING2.")))))

(defun test/ending-sub-string-p ()
  "Runs all possible tests for (ENDING-SUB-STRING-P)."
  (let ((string1    "Hello")
        (substring1 "ll")
        (string2    "World")
        (substring2 "ld"))
    (cond
      ((sml:ending-sub-string-p string1 substring1)
        (error "(ENDING-SUB-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((sml:ending-sub-string-p string1 substring1 :ignore-case nil)
        (error "(ENDING-SUB-STRING-P) failed test on STRING1 with SUBSTRING1."))
      ((not (sml:ending-sub-string-p string2 substring2))
        (error "(ENDING-SUB-STRING-P) failed test on STRING2 with SUBSTRING2."))
      ((not (sml:ending-sub-string-p string2 substring2 :ignore-case nil))
        (error "(ENDING-SUB-STRING-P) failed test on STRING2 with SUBSTRING2.")))))

(defun test/prefix-p ()
  "Runs all possible tests for (PREFIX-P)."
    (cond
      ((sml:prefix-p '("Hello" "Column") "He")
        (error "(PREFIX-P) failed test on Hello and Column with He."))
      ((sml:prefix-p '("Helium" "Helix") "he" :ignore-case nil)
        (error "(PREFIX-P) failed test on Helium and Helix with He."))
      ((not (sml:prefix-p '("Helium" "Helix") "He"))
        (error "(PREFIX-P) failed test on Helium and Helix with He."))))

(defun test/suffix-p ()
  "Runs all possible tests for (SUFFIX-P)."
    (cond
      ((sml:suffix-p '("Hello" "Column") "lo")
        (error "(SUFFIX-P) failed test on Hello and Column with lo."))
      ((sml:suffix-p '("Huelix" "Helix") "IX" :ignore-case nil)
        (error "(SUFFIX-P) failed test on Huelix and Helix with IX."))
      ((not (sml:suffix-p '("Huelix" "Helix") "ix"))
        (error "(SUFFIX-P) failed test on Huelix and Helix with ix."))))

(defun test/substring-in-list-p ()
  "Runs all possible tests for (SUBSTRING-IN-LIST-P)."
  (cond
    ((sml:substring-in-list-p '("Hello" "world") "How")
      (error "(SUBSTRING-IN-LIST-P) failed test on list."))
    ((sml:substring-in-list-p '("Hello" "world") "HOW" :ignore-case nil)
      (error "(SUBSTRING-IN-LIST-P) failed test on list."))
    ((not (sml:substring-in-list-p '("Hello" "world") "world"))
      (error "(SUBSTRING-IN-LIST-P) failed test on list."))
    ((not (sml:substring-in-list-p '("Hello" "WORLD") "WORLD" :ignore-case nil))
      (error "(SUBSTRING-IN-LIST-P) failed test on list."))))
