(in-package :sml/tests)

;;;; The split-test.lisp file implements rigorous checks
;;;; for all variables, functions, etc. within split.lisp
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun test/split-string ()
  "Runs all possible tests for (SPLIT-STRING)."
  (cond
    ((not (sml:split-string "Hello world" #\Space))
     (error "(SPLIT-STRING) failed generic call."))
    ((not (sml:split-string "Hello world" #\Space :omit-nulls t :target-limit 8 :index-range '(4 8)))
     (error "(SPLIT-STRING) failed the first non-generic call."))))

(defun test/reverse-split-string ()
  "Runs all possible tests for (REVERSE-SPLIT-STRING)."
  (cond
    ((not (sml:reverse-split-string "Hello world" #\Space))
     (error "(REVERSE-SPLIT-STRING) failed generic call."))
    ((not (sml:reverse-split-string "Hello world" #\Space :omit-nulls t :target-limit 8))
     (error "(REVERSE-SPLIT-STRING) failed the non-generic call."))))

(defun test/split-with-omit-nulls ()
  "Runs all possible tests for (SPLIT-WITH-OMIT-NULLS)."
  (when (not (sml:split-with-omit-nulls "Hello world" #\Space))
    (error "(SPLIT-WITH-OMIT-NULLS) failed generic call.")))

(defun test/string-to-words ()
  "Runs all possible tests for (STRING-TO-WORDS)."
  (cond
    ((not (sml:string-to-words "Hello world"))
      (error "(STRING-TO-WORDS) failed generic call."))
    ((not (sml:string-to-words "Hello world this is common lisp" :target-limit 2))
      (error "(STRING-TO-WORDS) failed non-generic call."))))

(defun test/split-by-lines ()
  "Runs all possible tests for (SPLIT-BY-LINES)."
  (let ((string1 "Hello
                  world"))
    (cond
       ((not (sml:split-by-lines string1))
          (error "(SPLIT-BY-LINES) failed generic call."))
       ((not (sml:split-by-lines string1 :omit-nulls t))
          (error "(SPLIT-BY-LINES) failed non-generic call.")))))
