(defpackage :sml/tests
  (:use :common-lisp :asdf :cl-ppcre)
  (:export
    ;; check-test.lisp
    #:test/empty-p
    #:test/exists-p
    #:test/digit-p
    #:test/alphanumeric-p
    #:test/has-alphanumeric-p
    #:test/alpha-p
    #:test/has-alpha-p
    #:test/letter-p
    #:test/has-letter-p
    #:test/ascii-char-p
    #:test/ascii-p
    #:test/uppercase-p
    #:test/lowercase-p
    #:test/sub-string-p
    #:test/sub-string-index-p
    #:test/scan-string-p
    #:test/starting-char-p
    #:test/ending-char-p
    #:test/starting-sub-string-p
    #:test/ending-sub-string-p
    #:test/prefix-p
    #:test/suffix-p
    #:test/substring-in-list-p

    ;; concatenation-test.lisp
    #:test/concatenate-string
    #:test/join
    #:test/join-lines
    #:test/append-string
    #:test/prepend-string
    #:test/ensure-first-char
    #:test/ensure-last-char
    #:test/repeated-concatenation
    #:test/add-prefix
    #:test/add-suffix
    #:test/pad-right
    #:test/pad-left
    #:test/pad-center

    ;; split-test.lisp
    #:test/split-string
    #:test/reverse-split-string
    #:test/split-with-omit-nulls
    #:test/string-to-words
    #:test/split-by-lines

    ;; substring-test.lisp
    #:test/substring
    #:test/first-substring
    #:test/last-substring
    #:test/rest-of-substring
    #:test/nth-of-substring
    #:test/insert-substring
    #:test/replace-substring-with-string
    #:test/replace-first-substring-with-string
    #:test/find-prefix
    #:test/find-suffix
    #:test/count-substring

    ;; trim-test.lisp
    #:test/whitespace
    #:test/trim-left
    #:test/trim-right
    #:test/trim
    #:test/collapse-whitespace
    #:test/strip-first-char
    #:test/strip-last-char
    #:test/strip-first-char-p
    #:test/strip-last-char-p
    #:test/shorten
    #:test/fit-string
    #:test/strip-punctuation

    ;; sml-test.lisp
    #:test/sml))
