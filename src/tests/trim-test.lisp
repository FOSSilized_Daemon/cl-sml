(in-package :sml/tests)

;;;; The trim-test.lisp file implements rigorous checks
;;;; for all variables, functions, etc. within trim.lisp
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun test/whitespace ()
  "Runs all possible tests for *WHITESPACE*."
  (when (not sml:*whitespace*)
    (error "*WHITESPACE* failed tests.")))

(defun test/trim-left ()
  "Runs all possible tests for (TRIM-LEFT)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "My fourth string.     ")
        (string5 "      My fifth string.")
        (string6 "      My sixth string.      ")
        (string7 "      My seventh string.    ")
        (string8 "      My eigth string.
                 "))
    (cond
      ((not (sml:trim-left string1))
        (error "(TRIM-LEFT) failed test on STRING1."))
      ((not (sml:trim-left string2))
        (error "(TRIM-LEFT) failed test on STRING2."))
      ((not (sml:trim-left string3))
        (error "(TRIM-LEFT) failed test on STRING3."))
      ((not (sml:trim-left string4))
        (error "(TRIM-LEFT) failed test on STRING4."))
      ((not (sml:trim-left string5))
        (error "(TRIM-LEFT) failed test on STRING5."))
      ((not (sml:trim-left string6))
        (error "(TRIM-LEFT) failed test on STRING6."))
      ((not (sml:trim-left string7))
        (error "(TRIM-LEFT) failed test on STRING7."))
      ((not (sml:trim-left string8))
        (error "(TRIM-LEFT) failed test on STRING8.")))))

(defun test/trim-right ()
  "Runs all possible tests for (TRIM-RIGHT)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "My fourth string.     ")
        (string5 "      My fifth string.")
        (string6 "      My sixth string.      ")
        (string7 "      My seventh string.    ")
        (string8 "      My eigth string.
                 "))
    (cond
      ((not (sml:trim-right string1))
        (error "(TRIM-RIGHT) failed test on STRING1."))
      ((not (sml:trim-right string2))
        (error "(TRIM-RIGHT) failed test on STRING2."))
      ((not (sml:trim-right string3))
        (error "(TRIM-RIGHT) failed test on STRING3."))
      ((not (sml:trim-right string4))
        (error "(TRIM-RIGHT) failed test on STRING4."))
      ((not (sml:trim-right string5))
        (error "(TRIM-RIGHT) failed test on STRING5."))
      ((not (sml:trim-right string6))
        (error "(TRIM-RIGHT) failed test on STRING6."))
      ((not (sml:trim-right string7))
        (error "(TRIM-RIGHT) failed test on STRING7."))
      ((not (sml:trim-right string8))
        (error "(TRIM-RIGHT) failed test on STRING8.")))))

(defun test/trim ()
  "Runs all possible tests for (TRIM)."
  (let ((string1 "")
        (string2 nil)
        (string3 "My string.")
        (string4 "My fourth string.     ")
        (string5 "      My fifth string.")
        (string6 "      My sixth string.      ")
        (string7 "      My seventh string.    ")
        (string8 "      My eigth string.
                 "))
    (cond
      ((not (sml:trim string1))
        (error "(TRIM) failed test on STRING1."))
      ((not (sml:trim string2))
        (error "(TRIM) failed test on STRING2."))
      ((not (sml:trim string3))
        (error "(TRIM) failed test on STRING3."))
      ((not (sml:trim string4))
        (error "(TRIM) failed test on STRING4."))
      ((not (sml:trim string5))
        (error "(TRIM) failed test on STRING5."))
      ((not (sml:trim string6))
        (error "(TRIM) failed test on STRING6."))
      ((not (sml:trim string7))
        (error "(TRIM) failed test on STRING7."))
      ((not (sml:trim string8))
        (error "(TRIM) failed test on STRING8.")))))

(defun test/collapse-whitespace ()
  "Runs all possible tests for (COLLAPSE-WHITESPACE)."
  (when (not (sml:collapse-whitespace "Hello       world"))
    (error "(COLLAPSE-WHITESPACE) failed tests.")))

(defun test/strip-first-char ()
  "Runs all possible tests for (STRIP-FIRST-CHAR)."
  (when (not (sml:strip-first-char "Hello"))
    (error "(STRIP-FIRST-CHAR) failed tests.")))

(defun test/strip-last-char ()
  "Runs all possible tests for (STRIP-LAST-CHAR)."
  (when (not (sml:strip-last-char "Hello"))
    (error "(STRIP-LAST-CHAR) failed tests.")))

(defun test/strip-first-char-p ()
  "Runs all possible tests for (STRIP-FIRST-CHAR-P)."
  (cond
    ((sml:strip-first-char-p "Hello" "e")
      (error "(STRIP-FIRST-CHAR) failed test on generic call."))
    ((not (sml:strip-first-char-p "Hello" "H" :ignore-case nil))
      (error "(STRIP-FIRST-CHAR) failed test on non-generic call."))))

(defun test/strip-last-char-p ()
  "Runs all possible tests for (STRIP-LAST-CHAR-P)."
  (cond
    ((sml:strip-last-char-p "Hello" "l")
      (error "(STRIP-LAST-CHAR) failed test on generic call."))
    ((not (sml:strip-last-char-p "Hello" "o" :ignore-case nil))
      (error "(STRIP-LAST-CHAR) failed test on non-generic call."))))

(defun test/shorten ()
  "Runs all possible tests for (SHORTEN)."
  (cond
    ((not (sml:shorten "Hello world" 4))
      (error "(SHORTEN) failed test on generic call."))
    ((not (sml:shorten "Hello world" 4 :ellipsis "___"))
     (error "(SHORTEN) failed test on non-generic call."))))

(defun test/fit-string ()
  "Runs all possible tests for (FIT-STRING)."
  (cond
    ((not (sml:fit-string "Hello world, are you not tired?" 5 :ellipsis "___"))
      (error "(FIT-STRING) failed test on generic call."))
    ((not (sml:fit-string "Hello world, are you not tired?" 20 :target-character #\- :target-side :left))
      (error "(FIT-STRING) failed test on non-generic call."))))

(defun test/strip-punctuation ()
  "Runs all possible tests for (STRIP-PUNCTUATION)."
  (cond
    ((not (sml:strip-punctuation "Hello! World."))
      (error "(STRIP-PUNCTUATION) failed test on generic call."))
    ((not (sml:strip-punctuation "Hello! World." :replacement-character "-"))
      (error "(STRIP-PUNCTUATION) failed test on non-generic call."))))
