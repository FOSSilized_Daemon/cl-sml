(in-package :sml/tests)

;;;; The concatenation-test.lisp file implements rigorous checks
;;;; for all variables, functions, etc. within concatenation.lisp
;;;; in ANSI compliant Common Lisp. The entirety of the below
;;;; codebase is written to be easily understood and
;;;; reprogrammable from any ANSI compliant Common Lisp
;;;; implementation.

(defun test/concatenate-string ()
  "Runs all possible tests for (CONCATENATE-STRING)."
  (when (not (sml:concatenate-string "String1"
                                     "String2"
                                     "String3"
                                     "String4"
                                     "String5"))
    (error "(CONCATENATE-STRING) failed test.")))

(defun test/join ()
  "Runs all possible tests for (JOIN)."
  (when (not (sml:join '("String1"
                         "String2"
                         "String3"
                         "String4"
                         "String5") " "))
    (error "(JOIN) failed test.")))

(defun test/join-lines ()
  "Runs all possible tests for (JOIN-LINES)."
  (when (not (sml:join-lines '("String1"
                               "String2"
                               "String3"
                               "String4"
                               "String5")))
    (error "(JOIN) failed test.")))

(defun test/append-string ()
  "Runs all possible tests for (APPEND-STRING)."
  (when (not (sml:append-string "world" "Hello"))
    (error "(APPEND-STRING) failed test.")))

(defun test/prepend-string ()
  "Runs all possible tests for (APPEND-STRING)."
  (when (not (sml:append-string "Hello" "world"))
    (error "(PREPEND-STRING) failed test.")))

(defun test/ensure-first-char ()
  "Runs all possible tests for (ENSURE-FIRST-CHAR)."
  (let ((string1    "orld")
        (string2    "ello")
        (character1 "o")
        (character2 "h"))
  (cond
    ((sml:ensure-first-char string1 character1)
      (error "(ENSURE-FIRST-CHAR) failed test for STRING1 with CHARACTER1."))
    ((not (sml:ensure-first-char string2 character2))
      (error "(ENSURE-FIRST-CHAR) failed test for STRING2 with CHARACTER2.")))))

(defun test/ensure-last-char ()
  "Runs all possible tests for (ENSURE-LAST-CHAR)."
  (let ((string1    "world")
        (string2    "hell")
        (character1 "d")
        (character2 "o"))
  (cond
    ((sml:ensure-last-char string1 character1)
     (error "(ENSURE-LAST-CHAR) failed test for STRING1 with CHARACTER1."))
    ((not (sml:ensure-last-char string2 character2))
     (error "(ENSURE-LAST-CHAR) failed test for STRING2 with CHARACTER2.")))))

(defun test/repeated-concatenation ()
  "Runs all possible tests for (REPEATED-CONCATENATION)."
  (cond
    ((not (sml:repeated-concatenation "Hello"))
      (error "(REPEATED-CONCATENATION) failed test for generic call."))
    ((not (sml:repeated-concatenation "Hello" :repeat-times 10))
      (error "(REPEATED-CONCATENATION) failed test for non-generic call."))))

(defun test/add-prefix ()
  "Runs all possible tests for (ADD-PREFIX)."
  (when (not (sml:add-prefix '("Hello"
                               "World") "Re-"))
    (error "(ADD-PREFIX) failed tests.")))

(defun test/add-suffix ()
  "Runs all possible tests for (ADD-SUFFIX)."
  (when (not (sml:add-suffix '("Hello"
                               "World") "-ed"))
    (error "(ADD-SUFFIX) failed tests.")))

(defun test/pad-right ()
  "Runs all possible tests for (PAD-RIGHT)."
  (cond
    ((not (sml:pad-right "Hello world" 20))
      (error "(PAD-RIGHT) failed test for generic call."))
    ((not (sml:pad-right "Hello world" 20 :target-character #\.))
     (error "(PAD-RIGHT) failed test for non-generic call."))))

(defun test/pad-left ()
  "Runs all possible tests for (PAD-LEFT)."
  (cond
    ((not (sml:pad-left "Hello world" 20))
      (error "(PAD-LEFT) failed test for generic call."))
    ((not (sml:pad-left "Hello world" 20 :target-character #\.))
     (error "(PAD-LEFT) failed test for non-generic call."))))

(defun test/pad-center ()
  "Runs all possible tests for (PAD-CENTER)."
  (cond
    ((not (sml:pad-center "Hello world" 20))
      (error "(PAD-CENTER) failed test for generic call."))
    ((not (sml:pad-center "Hello world" 20 :target-character #\.))
     (error "(PAD-CENTER) failed test for non-generic call."))))
