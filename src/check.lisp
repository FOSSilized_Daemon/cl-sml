(in-package :sml)

;;;; The check.lisp file implements variables, functions, and other
;;;; functionality for performing string checks in ANSI compliant
;;;; Common Lisp. The entirety of the below codebase is written
;;;; to be easily understood and reprogrammable from any ANSI
;;;; compliant Common Lisp implementation.

(defun empty-p (target-string)
  "Determines if TARGET-STRING is empty or just whitespace."
  (or (null target-string)
      (string-equal "" (trim target-string))))

(defun exists-p (target-string)
  "Determines if TARGET-STRING exists."
  (and (stringp target-string)
       (not (empty-p target-string))))

(defun digit-p (target-string)
  "Determines if TARGET-STRING contains at least one digit."
  (when (not (empty-p target-string))
    (loop for target-character across target-string
      when (digit-char-p target-character)
        do (return-from digit-p t))))

(defun alphanumeric-p (target-string &key (letters-digits nil))
  "Determines if TARGET-STRING contains at last one character
   and if all characters are alphanumeric or letters and digits."
   (cond
     ((not letters-digits)
        (when (ppcre:scan "^[a-zA-Z0-9 ]+$" target-string)
          t))
     ((when (ppcre:scan "^[\\p{L}a-zA-Z0-9 ]+$" target-string)
        t))))

(defun has-alphanumeric-p (target-string)
  "Determines if TARGET-STRING has at least one alphanumeric
   character."
  (when (not (empty-p target-string))
    (loop for target-character across target-string
      when (alphanumeric-p (string target-character))
        do (return-from has-alphanumeric-p t))))

(defun alpha-p (target-string)
  "Determines if TARGET-STRING contains at last one character
   and if all characters are alphabetic."
  (when (ppcre:scan "^[a-zA-Z ]+$" target-string)
    t))

(defun has-alpha-p (target-string)
  "Determines if TARGET-STRING has at least one alphabetic
   character."
  (when (not (empty-p target-string))
    (loop for target-character across target-string
      when (alpha-p (string target-character))
        do (return-from has-alpha-p t))))

(defun letter-p (target-string)
  "Determines if TARGET-STRING contains only letters."
  (when (ppcre:scan "^\\p{L}+$" target-string)
    t))

(defun has-letter-p (target-string)
  "Determines if TARGET-STRING containes at least one letter."
  (when (not (empty-p target-string))
    (loop for target-character across target-string
      when (letter-p (string target-character))
        do (return-from has-letter-p t))))

(defun ascii-char-p (target-character)
  "Determines if TARGET-CHARACTER is an ASCII character."
  (when (< (char-code target-character) 128)
    t))

(defun ascii-p (target-characters)
  "Determines if TARGET-CHARACTER is an ASCII character or, if
   TARGET-CHARACTER is a string, determines if each character
   is an ASCII character."
  (typecase target-characters
    (character
      (ascii-char-p target-characters))
    (string
      (every #'ascii-char-p target-characters))))

(defun uppercase-p (target-string)
  "Determines if TARGET-STRING is full of uppercase alphabetical
   characters."
  (when (has-letter-p target-string)
    (every (lambda (target-character)
             (if (alpha-char-p target-character)
               (upper-case-p target-character)
               t))
           target-string)))

(defun lowercase-p (target-string)
  "Determines if TARGET-STRING is full of lowercase alphabetical
   characters."
  (when (has-letter-p target-string)
    (every (lambda (target-character)
             (if (alpha-char-p target-character)
               (lower-case-p target-character)
               t))
           target-string)))

(defun sub-string-p (target-string sub-string &key (ignore-case t))
  "Determines if SUB-STRING is in TARGET-STRING in a case-sensitive
   manner (disabled by default)."
  (let ((new-target-string (if (not ignore-case)
                             sub-string
                             (string-downcase target-string)))
        (new-sub-string (if (not ignore-case)
                          sub-string
                          (string-downcase sub-string))))
    (when (and (not (empty-p new-target-string))
               (search new-sub-string target-string))
      t)))

(defun sub-string-index-p (target-string sub-string &key (index-range nil)
                                                         (ignore-case t))
  "Determines if SUB-STRING is either the last character of
   TARGET-STRING or found at INDEX-RANGE in a case-sensitive
   manner (disabled by default)."
  (let ((string-test-function (if (not ignore-case)
                       #'string=
                       #'string-equal)))

    (if (not index-range)
      (funcall string-test-function (subseq target-string (- (length target-string) 1)) sub-string)
      (funcall string-test-function (apply #'subseq target-string index-range) sub-string))))


(defun scan-string-p (target-string sub-string &key (index-range nil))
  "Determines if SUB-STRING is in TARGET-STRING and
   either returns SUB-STRING or, if INDEX-RANGE is true,
   returns the index range of SUB-STRING."
  (if (not index-range)
    (progn
      (when (ppcre:scan-to-strings sub-string target-string)
        sub-string))
    (ppcre:scan sub-string target-string)))

(defun starting-char-p (target-string target-character &key (ignore-case t))
  "Determines if TARGET-STRING starts with TARGET-CHARACTER
   in a case-sensitive manner (disabled by default)."
  (sub-string-index-p target-string target-character :index-range '(0 1) :ignore-case ignore-case))

(defun ending-char-p (target-string target-character &key (ignore-case t))
  "Determines if TARGET-STRING ends with TARGET-CHARACTER
   in a case-sensitive manner (disabled by default)."
  (sub-string-index-p target-string target-character :ignore-case ignore-case))

(defun starting-sub-string-p (target-string sub-string &key (ignore-case t))
  "Determines if TARGET-STRING starts with SUB-STRING in
   a case-sensitive manner (disabled by default)."
  (let ((sub-string-length    (length sub-string))
        (string-test-function (if (not ignore-case)
                                #'string=
                                #'string-equal)))
    (when (not (< (length target-string) sub-string-length))
        (funcall string-test-function target-string sub-string :start1 0 :end1 sub-string-length))))

(defun ending-sub-string-p (target-string sub-string &key (ignore-case t))
  "Determines if TARGET-STRING ends with SUB-STRING in a
   case-sensitive manner (disabled by default)."
  (let ((target-string-length (length target-string))
        (sub-string-length    (length sub-string))
        (string-test-function (if (not ignore-case)
                                #'string=
                                #'string-equal)))
    (when (not (< target-string-length sub-string-length))
        (funcall string-test-function target-string sub-string :start1 (- target-string-length sub-string-length)))))

(defun prefix-p (string-list target-prefix &key (ignore-case t))
  "Determines if TARGET-PREFIX prefixes all strings in
   STRING-LISTm in a case-sensitive manner (disabled
   by default), and if so returns TARGET-PREFIX."
  (when (every (lambda (target-string)
                 (starting-sub-string-p target-string target-prefix :ignore-case ignore-case))
               string-list)
    target-prefix))

(defun suffix-p (string-list target-suffix &key (ignore-case t))
  "Determines if TARGET-SUFFIX is the suffix for all
   strings in STRING-LIST, in a case-sensitive manner
   (disabled by default), and if so returns TARGET-SUFFIX."
  (when (every (lambda (target-string)
                 (ending-sub-string-p target-string target-suffix :ignore-case ignore-case))
               string-list)
    target-suffix))

(defun substring-in-list-p (string-list target-string &key (ignore-case t))
  "Determines if TARGET-STRING is a member of STRING-LIST
   in a case-sensitive manner (disabled by default)."
  (when (member target-string string-list
                :test (if (not ignore-case)
                        #'string=
                        #'string-equal))
    t))
