(in-package :sml)

;;;; The case.lisp file implements variables, functions, and other
;;;; functionality for performing string case operations in ANSI
;;;; compliant Common Lisp. The entirety of the below codebase
;;;; is written to be easily understood and reprogrammable from
;;;; any ANSI compliant Common Lisp implementation.

(defun uppercase (target-string)
  "Returns an uppercase version of TARGET-STRING
   if it is not already uppercase."
  (when (not (and (empty-p target-string)
                  (uppercase-p target-string)))
    (string-upcase target-string)))

(defun lowercase (target-string)
  "Returns a lowercase version of TARGET-STRING
   if it is not already lowercase."
  (when (not (and (empty-p target-string)
                  (lowercase-p target-string)))
    (string-downcase target-string)))

(defun capitalize (target-string)
  "Returns a capitalized version of
   TARGET-STRING."
   (when (not (empty-p target-string))
     (let ((capitalized-string nil))
       (setf capitalized-string (concatenate-string
                                    (uppercase (subseq target-string 0 1))
                                    (rest-of-substring target-string)))
       capitalized-string)))

(defun decapitalize (target-string)
  "Returns a decapitalized version
   of TARGET-STRING."
   (when (not (empty-p target-string))
     (let ((decapitalized-string nil))
       (setf decapitalized-string (concatenate-string
                                    (lowercase (subseq target-string 0 1))
                                    (rest-of-substring target-string)))
       decapitalized-string)))

(defun decase (target-string &key (replacement-character ""))
  "Returns TARGET-STRING in all lowercase
   and delimited by REPLACEMENT-CHARACTER
   (disabled by default)."
  (when (not (empty-p target-string))
    (let ((new-string nil))
      (loop for current-character across (trim target-string)
            while (not (eql current-character nil))
              when (not (or (eql current-character nil)
                            (eql current-character #\Space)))
              do (setf new-string
                       (concatenate-string
                         (when (not (string-equal new-string nil))
                           new-string)
                         (lowercase current-character) replacement-character)))
      (trim new-string))))

(defun camel-case (target-string &key (target-case :uppercase))
  "Returns TARGET-STRING in camelcase based
   on TARGET-CASE (:UPPERCASE by default)."
   (when (not (empty-p target-string))
     (let ((last-character nil)
           (new-string     nil)
           (case-test-function (case target-case
                                 (:uppercase
                                   #'uppercase)
                                 (:lowercase
                                   #'lowercase)))
           (inverse-case-test-function (case target-case
                                         (:uppercase
                                           #'lowercase)
                                         (:lowercase
                                           #'uppercase))))
       (loop for current-character across (trim target-string)
          while (not (eql current-character nil))
            do (if (or (eql last-character #\Space)
                       (eql last-character nil))
                 (progn
                   (when (not (eql current-character #\Space))
                     (setf new-string (concatenate-string
                                        (when (not (string-equal new-string nil))
                                          new-string)
                                        (funcall case-test-function (string current-character)))))
                   (setf last-character current-character))
                 (progn
                   (when (not (eql current-character #\Space))
                     (setf new-string (concatenate-string
                                        (when (not (string-equal new-string nil))
                                          new-string)
                                        (funcall inverse-case-test-function (string current-character)))))
                   (setf last-character current-character))))
       new-string)))

(defun dot-case (target-string)
  "Returns a dot-case version of TARGET-STRING."
  (decase target-string :replacement-character "."))

(defun param-case (target-string)
  "Returns a param-case version of TARGET-STRING."
  (decase target-string :replacement-character "-"))

(defun pascal-case (target-string)
  "Returns a pascal-case version of TARGET-STRING."
  (camel-case target-string :target-case :uppercase))

(defun path-case (target-string)
  "Returns a path-case version of TARGET-STRING."
  (decase target-string :replacement-character "/"))

(defun sentence-case (target-string)
  "Returns a sentence-case version of TARGET-STRING."
  (capitalize (decase target-string)))

(defun snake-case (target-string)
  "Returns a snake-case version of TARGET-STRING."
  (decase target-string :replacement-character "_"))

(defun swap-case (target-string)
  "Returns an inverse-case version of TARGET-STRING."
  (when (not (empty-p target-string))
    (let ((new-string nil))
      (loop for current-character across (trim target-string)
            while (not (eql current-character nil))
              do (if (uppercase-p (string current-character))
                   (progn
                     (setf new-string (concatenate-string
                                        (when (not (string-equal new-string nil))
                                          new-string)
                                        (lowercase (string current-character)))))
                   (progn
                     (setf new-string (concatenate-string
                                        (when (not (string-equal new-string nil))
                                          new-string)
                                        (uppercase (string current-character)))))))
      new-string)))

(defun constant-case (target-string)
  "Returns a constant-case version of TARGET-STRING."
  (uppercase (snake-case target-string)))

(defun title-case (target-string)
  "Returns a title-case version of TARGET-STRING."
  (when (not (empty-p target-string))
     (let ((last-character nil)
           (new-string     nil))
       (loop for current-character across (trim target-string)
          while (not (eql current-character nil))
            do (if (or (eql last-character #\Space)
                       (eql last-character nil))
                 (progn
                   (setf new-string (concatenate-string
                                      (when (not (string-equal new-string nil))
                                        new-string)
                                      (uppercase (string current-character))))
                   (setf last-character current-character))
                 (progn
                   (setf new-string (concatenate-string
                                      (when (not (string-equal new-string nil))
                                        new-string)
                                      (lowercase (string current-character))))
                   (setf last-character current-character))))
     new-string)))

(defun header-case (target-string)
  "Returns a header-case version of TARGET-STRING."
  (when (not (empty-p target-string))
     (let ((last-character nil)
           (new-string     nil))
       (loop for current-character across (trim target-string)
          while (not (eql current-character nil))
            do (if (or (eql last-character #\Space)
                       (eql last-character nil))
                 (progn
                   (setf new-string (concatenate-string
                                      (when (not (string-equal new-string nil))
                                        new-string)
                                      (uppercase (string current-character))))
                   (setf last-character current-character))
                 (progn
                   (setf new-string (concatenate-string
                                      (when (not (string-equal new-string nil))
                                        new-string)
                                      (lowercase (string current-character))))
                   (setf last-character current-character))))
     (ppcre:regex-replace-all " " (trim new-string) "-" :preserve-case t))))
