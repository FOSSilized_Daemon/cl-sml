(defpackage :sml
  (:use :common-lisp :asdf :cl-ppcre)
  (:export
    ;; case.lisp
    #:uppercase
    #:lowercase
    #:capitalize
    #:decapitalize
    #:decase
    #:camel-case
    #:dot-case
    #:param-case
    #:pascal-case
    #:path-case
    #:sentence-case
    #:snake-case
    #:swap-case
    #:constant-case
    #:title-case
    #:header-case

    ;; check.lisp
    #:empty-p
    #:exists-p
    #:digit-p
    #:alphanumeric-p
    #:has-alphanumeric-p
    #:alpha-p
    #:has-alpha-p
    #:letter-p
    #:has-letter-p
    #:ascii-char-p
    #:ascii-p
    #:uppercase-p
    #:lowercase-p
    #:sub-string-p
    #:sub-string-index-p
    #:scan-string-p
    #:starting-char-p
    #:ending-char-p
    #:starting-sub-string-p
    #:ending-sub-string-p
    #:prefix-p
    #:suffix-p
    #:substring-in-list-p

    ;; concatention.lisp
    #:concatenate-string
    #:join
    #:join-lines
    #:append-string
    #:prepend-string
    #:ensure-first-char
    #:ensure-last-char
    #:repeated-concatenation
    #:add-prefix
    #:add-suffix
    #:pad
    #:pad-right
    #:pad-left
    #:pad-center

    ;; split.lisp
    #:split-string
    #:reverse-split-string
    #:split-with-omit-nulls
    #:string-to-words
    #:words-to-string
    #:split-by-lines

    ;; substring.lisp
    #:substring
    #:first-substring
    #:last-substring
    #:rest-of-substring
    #:nth-of-substring
    #:insert-substring
    #:shorten
    #:replace-substring-with-string
    #:replace-first-substring-with-string
    #:find-prefix
    #:find-suffix
    #:count-substring

    ;; trim.lisp
    #:*whitespace*
    #:trim-left
    #:trim-right
    #:trim
    #:collapse-whitespace
    #:strip-first-char
    #:strip-last-char
    #:strip-first-char-p
    #:strip-last-char-p
    #:fit-string
    #:strip-punctuation))
